# SL Comunicação 
Este é um plano de comunicação experimental acadêmico do curso bacharelado em Comunicação Organizacional. Cujo objetivo é promover questionamentos sobre as manifestações sociais no espaço cibernético, trazendo fatores de interesse público sobre causas Políticas, Econômicas, Culturais e Individuais, que é acionada com o uso do computador e assim manifestar consciência do ser político que cada pessoa é enquanto usuário da tecnologia.

